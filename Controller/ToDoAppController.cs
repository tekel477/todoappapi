﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoAppApi.Model;

namespace ToDoAppApi.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoAppController : ControllerBase
    {
        private readonly ToDoAppContext _toDoAppContext;

        public ToDoAppController(ToDoAppContext toDoAppContext)
        {
            _toDoAppContext = toDoAppContext;
        }

        [HttpGet]
        public IEnumerable<Mission> Get()
        {
            return _toDoAppContext.Mission;

        }
        [HttpGet("{id}", Name = "GET")]
        public Mission Get(int id)
        {
            return _toDoAppContext.Mission.SingleOrDefault(x => x.Id == id);
        }

        [HttpPost]
        public void Post([FromBody] Mission mission)
        {
            _toDoAppContext.Mission.Add(mission);
            _toDoAppContext.SaveChanges();

        }
        [HttpPut("{id}")]
        public void Put(int id,[FromBody] Mission mission)
        {
            mission.Id = id;
            _toDoAppContext.Mission.Update(mission);
            _toDoAppContext.SaveChanges();
        }

        [HttpDelete("{id}")]

        public void Delete(int id)
        {
            var item = _toDoAppContext.Mission.FirstOrDefault(x => x.Id == id);
        }


       


    }
}