﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoAppApi.Model
{
    public class User : IdentityUser
    {
        
        [MaxLength(100)]
        public string  Name { get; set; }
        [MaxLength(100)]
        public string Surname { get; set; }
        [MaxLength(100)]
        public override string Email { get; set; }
        [MaxLength(200)]
        [NotMapped]
        public string FullName
        {
            get
            {
                return Name + " " + Surname;
            }
        }
        public bool Deleted { get; set; }

        public ICollection<Mission> Missions { get; set; }




    }
}
