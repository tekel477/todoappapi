﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoAppApi.Model
{
    public class ToDoAppContext : IdentityDbContext<User>
    {
        public ToDoAppContext(DbContextOptions<ToDoAppContext> options) : base(options)
        {

        }
        public virtual DbSet<Mission> Mission { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.HasDefaultSchema("dbo");

            builder.Entity<User>().ToTable("User", "Users");
            builder.Entity<Mission>().ToTable("Mission", "Missions");


        }

    }

}


